<!DOCTYPE html>
<html>
<head>
    <title>Bài tập pdoweb</title>
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css" style="text/css" rel="stylesheet">
</head>
<body  class="login-screen-bg">
<?php
    require('./validate1.php');
    require('./connect.php');
    $error = array();
    $data = array();
    if (!empty($_POST['dangky'])) {
        $data['email'] = $_POST['email'] ?? '';
        $data['password'] = $_POST['password'] ?? '';
        $data['password_confirm'] = $_POST['password_confirm'] ?? '';
        if ($data['email'] == '') {
                $error['email'] = 'Bạn chưa nhập email';
            } elseif (!is_email($data['email'])) {
                $error['email'] = 'Email không đúng định dạng';
            }
        if ($data['password'] == '') {
            $error['password'] = 'Bạn chưa nhập mật khẩu';
        } elseif (!is_password($data['password'])) {
            $error['password'] = 'password không đúng định dạng';
        }
        if ($data['password_confirm'] == '') {
            $error['password_confirm'] = 'Bạn chưa nhập lại mật khẩu';
        } elseif ($data['password_confirm'] != $data['password']) {
            $error['password_confirm'] = 'password không khớp';
        }
    }
    if (empty($error) && isset($_POST['dangky'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql ="INSERT INTO users(mail_address, password) VALUES ('$email','$password')";
        $conn->exec($sql);
    }
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 well well-sm col-md-offset-4">
            <legend><i class="glyphicon glyphicon-globe"></i> Đăng ký thành viên!</legend>
                <form method="post" class="form" role="form">
                    <input class="form-control" name="email" placeholder="Email" type="text" />
                    <p style="color: red;"><?php echo isset($error['email']) ? $error['email'] : ''; ?></p>
                    <input class="form-control" name="password" placeholder="Mật khẩu" type="password" />
                    <p style="color: red;"><?php echo isset($error['password']) ? $error['password'] : ''; ?></p>
                    <input class="form-control" name="password_confirm" placeholder="Nhập lại mật khẩu" type="password" />
                    <p style="color: red;"><?php echo isset($error['password_confirm']) ? $error['password_confirm'] : ''; ?></p>
                    <input class="btn btn-lg btn-primary btn-block" type="submit" name="dangky" value="Đăng ký" />
                </form>
        </div>
    </div>
</div>
</body>
</html>