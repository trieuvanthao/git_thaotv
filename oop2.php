<?php
abstract class Supervisor 
{
    protected $slogan;
    abstract public function saySloganOutLoud();
}

interface Boss 
{
    public function checkValidSlogan();
}

trait Active
{
    public function defindYourSelf() {
        return get_class($this);
    }
}

class EasyBoss extends Supervisor implements Boss
{
    use Active;
    public function setSlogan($text) {
        $this->slogan = $text;
    }

    public function saySloganOutLoud() {
        return $this->slogan;
    }

    public function checkValidSlogan() {
        if (strpos($this->slogan, 'before') !== false || strpos($this->slogan, 'after') !== false) {
            return true;
        }
        return false;
    }
}

class UglyBoss extends Supervisor implements Boss
{
    use Active;
    public function setSlogan($text) {
        $this->slogan = $text;
    }

    public function saySloganOutLoud() {
        return $this->slogan;
    }

    public function checkValidSlogan() {
        if (strpos($this->slogan, 'before') !== false && strpos($this->slogan, 'after') !== false) {
            return true;
        }
        return false;
    }
}
$easyBoss = new EasyBoss();
$uglyBoss = new UglyBoss();
$easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');
$uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');

$easyBoss->saySloganOutLoud(); 
echo "<br>";
$uglyBoss->saySloganOutLoud(); 

echo "<br>";

var_dump($easyBoss->checkValidSlogan());
echo "<br>";
var_dump($uglyBoss->checkValidSlogan());

echo "<br>";

echo 'I am ' . $easyBoss->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $uglyBoss->defindYourSelf(); 
?>