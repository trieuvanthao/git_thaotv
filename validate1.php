<?php
// Kiểm tra định dạng tên
function is_password($password)
{
    return (preg_match("/[a-zA-Z0-9]{6,25}/", $password));
}

// Kiểm tra định dạng email
function is_email($email)
{
    return (preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email));
}
?>