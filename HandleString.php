<?php
class HandleString 
{
    public $check1; 
    public $check2;
    /**
     * check string
     *
     * @return     true/false
     */
    public function checkValidString($text) 
    {
        if ($text === '') {
            return true;
        }
        if (strlen($text) >= 50 && strpos($text, 'after') === false) {
            return true;
        }
        if (strpos($text, 'before') !== false && strpos($text, 'after') === false) {
            return true;
        }
            return false;
    }

    /**
     * read file
     *
     * @return     true/false
     */
    public function readFile($file) 
    {
        if (file_exists($file))
        {
            $string  = @fopen($file, "r");
            return fread($string, filesize($file));
        }
            return false;
    }
}
//creat new object1
$object1 = new HandleString();
if ($object1->readFile('file1.txt') !== false) {
    $object1->check1 = $object1->checkValidString($object1->readFile('file1.txt'));
} else {
    echo 'file này không tồn tại';
}
echo "</br>";
if ($object1->readFile('file2.txt') !== false) {
    $object1->check2 = $object1->checkValidString($object1->readFile('file2.txt'));
} else {
    echo 'file này không tồn tại';
}
?>