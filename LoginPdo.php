<?php 
session_start();
require('./connect.php');
    if (isset($_POST['login'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $stmt = $conn->prepare("SELECT * FROM `users` WHERE mail_address = '$email' and password = '$password'");
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($results) > 0) {
            $_SESSION['name'] = $email;
        } else {
            echo'đăng nhập thất bại';
        }
        if (isset($_SESSION['name'])){
            header('Location:./LoginSuccessPdo.php');
        }
        if (isset($_POST['remember'])) {
            setcookie('email', $email, time() + 10, '/', '', 0, 0);
            setcookie('password', $password, time() + 10, '/', '', 0, 0);
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>bài tập pdoweb</title>
    <meta charset="utf-8">
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css" style="text/css" rel="stylesheet">
</head>
<body>
<?php
    $error = array();
    $data = array();
    if (!empty($_POST['login'])) {
        $data['email'] = $_POST['email'] ?? '';
        $data['password'] = $_POST['password'] ?? '';
        require('./validate1.php');
        if (empty($data['email'])) {
                $error['email'] = 'Bạn chưa nhập email';
            } elseif (!is_email($data['email'])) {
                $error['email'] = 'Email không đúng định dạng';
            }
        if (empty($data['password'])) {
            $error['password'] = 'Bạn chưa nhập mật khẩu';
        } elseif (!is_password($data['password'])) {
            $error['password'] = 'password không đúng định dạng';
        }
    }
?>
<div class="container-fluid">
    <div class="row-fluid" >
    <div class="col-md-offset-4 col-md-4" id="box">
        <h2>Đăng nhập</h2>
        <hr>
        <form class="form-horizontal" method="post" id="login_form">
        <fieldset>
            <div class="form-group">
                <div class="col-md-12">
                <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input name="email" placeholder="Email" class="form-control" type="text" value="<?php echo $_COOKIE['email'] ?? '';?>">
                </div>
                <div class="input-group" style="color: red;"> 
                    <?php echo isset($error['email']) ? $error['email'] : ''; ?>
                </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input name="password" placeholder="password" class="form-control" type="password" value="<?php echo $_COOKIE['password'] ?? '';?>">
                    </div>
                    <div class="input-group" style="color: red;"> 
                        <?php echo isset($error['password']) ? $error['password'] : ''; ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="input-group">
                        <label>Remember me</label>
                        <input type="checkbox" name="remember" value="" <?php if(isset($_COOKIE['email'])) echo "checked";?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input type="submit" class="btn btn-md btn-danger pull-right" name="login" value="Login" />
                 </div>
            </div>
        </fieldset>
        </form>
    </div>
  </div>
</div>               
</body>
</html>