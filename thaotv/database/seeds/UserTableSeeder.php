<?php
use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('vi_VN');
        User::truncate();
        for ($i=0; $i < 100 ; $i++) { 
            $user = User::create([
                'mail_address' => $faker->email,
                'password' => Hash::make($faker->password),
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->e164PhoneNumber,
                'role' => rand(1,2),
            ]);
        }
    }
}
