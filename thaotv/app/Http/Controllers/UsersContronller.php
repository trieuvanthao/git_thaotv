<?php

namespace App\Http\Controllers;

use App\Jobs\SendWelcomeEmail;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use App\Mail\WelcomeEmail;
use Mail;

class UsersContronller extends Controller
{
    protected $users;

    public function __construct(User $users)
    {
        $this->users = $users;
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $input = $request->all();
        $user = $this->users->getUser($input);
        return view('user.list_user',['user'=>$user]);
    }
    public function store(CreateUserRequest $request)
    {
        $users = $this->users->createUser($request->all());
        dispatch(new SendWelcomeEmail($users));
        $request->session()->flash('status', 'Thêm người dùng thành công!');
        return redirect()->route('users.index');
    }
    public function create()
    {
        $this->authorize('admin');
        return view('user.add_user');
    }
}
