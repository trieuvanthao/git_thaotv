<?php
namespace App\Facades;

class Helper 
{
    public function toUpperCase($str)
    {
        return mb_convert_case($str, MB_CASE_UPPER, "UTF-8");
    }
}