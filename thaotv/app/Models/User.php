<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;
    const ROLE_ADMIN = 1;
    const ROLE_STAFF = 2;
    protected $table = 'users';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'mail_address', 'address', 'phone', 'password','role',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    //public function 
    public function getUser(array $input)
    {
        $users = User::orderBy('mail_address', 'ASC');
        if (isset($input['mail_address'])) {
            $users->where('mail_address', 'like', '%'.$input['mail_address'].'%');
        }
        if (isset($input['name'])) {
            $users->where('name', 'like', '%'.$input['name'].'%');
        }
        if (isset($input['address'])) {
            $users->where('address', 'like', '%'.$input['address'].'%');
        }
        if (isset($input['phone'])) {
            $users->where('phone', $input['phone']);
        }
        return $users->paginate(20);
    }
    public function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        return User::create($data);
    }
}


