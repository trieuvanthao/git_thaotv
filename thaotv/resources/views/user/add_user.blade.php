@extends('layouts.default')
@section('title', __('Thêm mới người dùng'))
@section('content')
    <div class="container">
		<form method="POST" action="{{ route('users.store') }}">
			{{csrf_field()}}
            {{-- @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}
			<div class="form-group">
				<label for="exampleInputEmail1" style="@if($errors->has('name')) {{'color: red;'}} @endif">Tên</label>
				<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Tên" name="name" style="@if($errors->has('name')) {{'border: solid 1px red;'}} @endif">
				@if($errors->has('mail_address'))
                    <p style="color: red">{{ $errors->first('name') }}</p>
                @endif
			</div>
			<div class="form-group ">
				<label for="exampleInputEmail1" style="@if($errors->has('mail_address')) {{'color: red;'}} @endif">Email address</label>
				<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email" name="mail_address" style="@if($errors->has('mail_address')) {{'border: solid 1px red;'}} @endif">
				@if($errors->has('mail_address'))
                    <p style="color: red">{{ $errors->first('mail_address') }}</p>
                @endif
			</div>
			<div class="form-group">
				<label style="@if($errors->has('address')) {{'color: red;'}} @endif">Address</label>
				<textarea placeholder="Enter Address Here.." rows="3" class="form-control" name="address" style="@if($errors->has('address')) {{'border: solid 1px red;'}} @endif"></textarea>
				@if($errors->has('mail_address'))
                    <p style="color: red">{{ $errors->first('address') }}</p>
                @endif
			</div>
			<div class="form-group">
				<label style="@if($errors->has('phone')) {{'color: red;'}} @endif">Phone Number</label>
				<input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="phone" style="@if($errors->has('phone')) {{'border: solid 1px red;'}} @endif">
				@if($errors->has('mail_address'))
                    <p style="color: red">{{ $errors->first('phone') }}</p>
                @endif
			</div>
			<div class="form-group">
				<label style="@if($errors->has('password')) {{'color: red;'}} @endif">Password</label>
				<input type="password" placeholder="Enter Password Here.." class="form-control" name="password" style="@if($errors->has('password')) {{'border: solid 1px red;'}} @endif">
				@if($errors->has('mail_address'))
                    <p style="color: red">{{ $errors->first('password') }}</p>
                @endif
			</div>
			<div class="form-group">
				<label style="@if($errors->has('password_confirmation')) {{'color: red;'}} @endif">Password confirm</label>
				<input type="password" placeholder="Enter Password Here.." class="form-control" name="password_confirmation" style="@if($errors->has('password_confirmation')) {{'border: solid 1px red;'}} @endif">
				@if($errors->has('mail_address'))
                    <p style="color: red">{{ $errors->first('password_confirmation') }}</p>
                @endif
			</div>
			<div class="form-group">
                <label for="sel1">Vai trò:</label>
                    <select class="form-control" id="sel1" name="role">
                         <option value="1">Quản trị viên</option>
                         <option value="2">Nhân viên</option>
                    </select>
            </div>
			<input type="submit" class="btn btn-lg btn-info" value="Thêm mới"></input>
		</form> 
@endsection