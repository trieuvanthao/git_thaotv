@extends('layouts.app')
@section('title', __('Danh sách người dùng'))
@section('content')
    <div class="example">
        <div class="container">
            <div class="row">
             <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <div class="jumbotrom">
                    <form method="GET" action="{{ route('users.index') }}">
                        {{csrf_field()}}
                        @if (session('status'))
                            <div class="alert alert-info">{{session('status')}}</div>
                        @endif
                        <label>Email</label>
                        <div class="form-group">
                            <input type="text" name="mail_address" class="form-control">
                        </div>
                        <label>Tên</label>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control">
                        </div>
                        <label>Địa chỉ</label>
                        <div class="form-group">
                            <input type="text" name="address" class="form-control">
                        </div>
                        <label>Phone</label>
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control">
                        </div>
                        <div class="">
                            <input type="submit" name="" value="Tìm kiếm" class="btn btn-lg btn-info">
                        </div>
                    </form>
                </div>
            </div>
            </div>
            <div class="row">
                <h2>Danh sách</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr style="text-align: center;">
                            <th>STT</th>
                            <th>Tên</th>
                            <th>Địa chỉ email</th>
                            <th>Địa chỉ</th>
                            <th>Số điện thoại</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user as $value)
                        <tr>
                            <td>{{ $user ->perPage() * ($user->currentPage()-1) + $loop->iteration }}</td>
                            <td>{{ Helper::toUpperCase($value->name) }}</td>
                            <td>{{ $value->mail_address }}</td>
                            <td>{{ $value->address }}</td>
                            <td>{{ $value->phone }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @can('admin')
                <a href="{{route('users.create')}}" class="btn btn-lg btn-info" >Thêm user</a>
                @endcan
            </div>
        </div>
    </div>
    {{$user->links()}}
@endsection